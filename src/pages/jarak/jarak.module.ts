import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JarakPage } from './jarak';

@NgModule({
  declarations: [
    JarakPage,
  ],
  imports: [
    IonicPageModule.forChild(JarakPage),
  ],
})
export class JarakPageModule {}
